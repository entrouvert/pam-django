all:
	true

TARGET=/usr/local/lib/pam-django

install:
	@echo Installing into /usr/local/lib/pam-django/ ..
	install -o root -T pam.conf /etc/pam.d/proftpd
	install -o root -d /usr/local/lib/pam-django/
	install -o root check-password settings.py $(TARGET)
	. /etc/authentic2/db.conf; sed -i "s/{PASSWORD}/$$DATABASE_PASSWORD/" $(TARGET)/settings.py
	@echo Please edit /usr/local/lib/pam-django/settings.py to set the authentic2 database password
	@make generate
    
generate:
	@echo Generating proftpd configuration...
	@. /etc/docbow/db;. /etc/docbow/secret; sudo -E -u docbow python generate-ftp-passwd.py /etc/proftpd/ftpd.passwd /etc/proftpd/ftpd.group /etc/proftpd/conf.d/docbow.conf  && /etc/init.d/proftpd restart

import os
import syslog
import sys
import subprocess

def auth_log(msg, priority=syslog.LOG_NOTICE):
    syslog.openlog(ident='pam-django', facility=syslog.LOG_AUTH)
    syslog.syslog(priority, msg)
    syslog.closelog()

def pam_sm_authenticate(pamh, flags, argv):
    auth_log('sys.path %r' % argv)
    argv = dict(arg.split('=', 1) for arg in argv[1:] if '=' in arg)
    try:
      user = pamh.get_user(None)
    except pamh.exception, e:
      return e.pam_result

    if not user:
        return pamh.PAM_USER_UNKNOWN

    # this fucking binding didn't support pam_get_pass so we rewrite it
    try:
        resp = pamh.conversation(pamh.Message(pamh.PAM_PROMPT_ECHO_OFF, 'Password:'))
    except pamh.exception, e:
        return e.pam_result
    env = os.environ.copy()
    if 'DJANGO_SETTINGS_MODULE' in argv:
        env['DJANGO_SETTINGS_MODULE'] = argv['DJANGO_SETTINGS_MODULE']
    ret = subprocess.call([argv['helper'], user, resp.resp],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
    if ret == 0:
        auth_log('login success')
        return pamh.PAM_SUCCESS
    auth_log('login failure')
    return pamh.PAM_AUTH_ERR

def pam_sm_setcred(pamh, flags, argv):
    auth_log("pam_sm_setcred")
    return pamh.PAM_SUCCESS

def pam_sm_acct_mgmt(pamh, flags, argv):
    auth_log("pam_sm_acct_mgmt")
    return pamh.PAM_SUCCESS

def pam_sm_open_session(pamh, flags, argv):
    auth_log("pam_sm_open_session")
    return pamh.PAM_SUCCESS

def pam_sm_close_session(pamh, flags, argv):
    auth_log("pam_sm_close_session")
    return pamh.PAM_SUCCESS

def pam_sm_chauthtok(pamh, flags, argv):
    auth_log("pam_sm_chauthtok")
    return pamh.PAM_SUCCESS


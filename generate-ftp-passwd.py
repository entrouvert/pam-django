#!/bin/env python

import os
import sys
import glob

os.environ['DJANGO_SETTINGS_MODULE'] = 'docbow_project.pw.settings'

from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from docbow_project.docbow.models import MailingList
from django.conf import settings

password_file, group_file, proftpd_conf = sys.argv[1:4]

with file(password_file, 'w') as f:
    for user in User.objects.all():
        print >>f, '%s:*:108:111::/srv/ftp:/bin/false' % user.username


with file(group_file, 'w') as f:
    for ml in MailingList.objects.all():
        name = ''
        print >>f, '%s:*:50:' % slugify(ml.name),
        users = [user.username for user in ml.recursive_members()]
        users = u','.join(users)
        print >>f, users

with file(proftpd_conf, 'w') as f:
    print >>f, '''
<Directory /srv/ftp/>
	<Limit WRITE>
	    AllowGroup greffe
	    DenyAll
	</Limit>
</Directory>
'''
    names = set()
    for ml in MailingList.objects.all():
        name = slugify(ml.name)
        names.add(str(name))
        path = '/srv/ftp/%s' % name
        if not os.path.exists(path):
            print 'Creating', path
            os.makedirs(path)
        print >>f, '''<Directory {path}>
    <Limit READ WRITE DIRS>
        AllowGroup OR greffe,{name}
        DenyAll
    </Limit>
</Directory>'''.format(name=name, path=path)
    for directory in glob.glob('/srv/ftp/*'):
        if not os.path.isdir(directory):
            continue
        if not os.path.basename(directory) in names:
            print 'Orphan directory', directory, ': not mailing exists for this directory'

    

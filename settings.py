DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
	'NAME': 'authentic2',
	'USER': 'authentic2',
	'PASSWORD': '{PASSWORD}',
	'HOST': 'localhost',
    }
}
INSTALLED_APPS=('django.contrib.auth',)
AUTHENTICATION_BACKENDS=('django.contrib.auth.backends.ModelBackend',
 'authentic2.backends.ldap_backend.LDAPBackend')
SECRET_KEY='xxx'
LDAP_AUTH_SETTINGS=[
        {
            "url": "ldaps://ldap.libre-entreprise.org/",
            "basedn": "o=entrouvert,ou=companies,o=libre-entreprise",
            "user_filter": "uid=%s",
            "groupsu": ["cn=ldapadmins,ou=groups,o=entrouvert,ou=companies,o=libre-entreprise"],
            "groupstaff": ["cn=ldapadmins,ou=groups,o=entrouvert,ou=companies,o=libre-entreprise"],
            "group_filter": "(&(uniqueMember={user_dn})(objectClass=legroup))",
            "transient": False,
            "realm": "entrouvert.org",
            "create_group": True,
            "group_mapping": [
               ["cn=ldapadmins,ou=groups,o=entrouvert,ou=companies,o=libre-entreprise", [ "Superutilisateur" ]]
            ],
            "attributes": ["sn", "givenName", "mail"]
         }
]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/cache/authentic2',
    },
}

